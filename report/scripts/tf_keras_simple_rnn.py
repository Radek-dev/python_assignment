"""
Simple tensorflow.keras recurrent neural network.
"""
import numpy as np
import pandas as pd
import seaborn as sns

from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import (
    explained_variance_score,
    mean_squared_error,
    mean_absolute_error,
    median_absolute_error,
    r2_score,
    max_error)

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, SimpleRNN

sns.set_style('darkgrid')
plt.rcParams.update({'font.size': 25})


def plot_series(a: np.array, b: np.array):
    """
    This plots series b after series a.
    """
    nans = np.empty(a.shape)
    nans.fill(np.nan)
    b = np.append(nans, b)
    plt.subplots(figsize=(30, 7))
    plt.plot(a, color='blue', label='Training Series')
    plt.plot(b, color='red', label='Test Series')
    plt.title('Google Training and Test Series - Scaled')
    plt.xlabel('Time')
    plt.ylabel('Google Stock Price - Scaled')
    plt.legend()
    plt.show()


def plot_results(a: np.array, b: np.array):
    """
    This plots two series with the same time index.
    """
    plt.subplots(figsize=(30, 10))
    plt.plot(a, color='red', label='Testing Series')
    plt.plot(b, color='green', label='Estimated Series - Validation')
    plt.title('Series - Testing and Estimated')
    plt.xlabel('Time')
    plt.ylabel('Google Stock Price')
    plt.legend()
    plt.show()


def plot_train_series_and_results(y_train: np.array, y_test: np.array, y_pred: np.array):
    plt.subplots(figsize=(30, 10))
    plt.plot(y_train, color='blue', label='Training Series')
    nans = np.empty(y_train.shape)
    nans.fill(np.nan)
    y_test = np.append(nans, y_test)
    plt.plot(y_test, color='red', label='Testing Series')
    y_pred = np.append(nans, y_pred)
    plt.plot(y_pred, color='green', label='Estimated Series - Validation')
    plt.title('Google Price Series - Training, Testing, Estimated')
    plt.xlabel('Time')
    plt.ylabel('Google Stock Price')
    plt.legend()
    plt.show()


def prepare_data(df: pd.DataFrame, plotting: bool = True):
    """
    Pass in single time series in data frame.
    You can use plotting to turn off verbosity.
    """
    lags, n = 3, round(df.shape[0])
    n_train = round(n * .8) - lags

    # Scale the features
    scaler = MinMaxScaler(feature_range=(0, 1))
    series_scaled = scaler.fit_transform(df.values)

    # prepare the lags and the shape of the data
    x, y = [], []
    for i in range(lags, n):
        x.append(series_scaled[i - lags:i, 0])
        y.append(series_scaled[i, 0])
    x, y = np.array(x), np.array(y)
    x = np.reshape(x, (x.shape[0], x.shape[1], 1))

    x_train, y_train = x[:n_train, :, :], y[:n_train]
    x_test, y_test = x[n_train:, :, :], y[n_train:]

    y_test = y_test.reshape(-1, 1)

    if plotting:
        plot_series(y_train, y_test)
    return x_train, y_train, x_test, y_test, scaler


def fit_rnn(x_train: np.array, y_train: np.array, print_out_layers: bool = True):
    """
    Pass in the X train series as 3 dimensional array.
    Pass in the y train series as 1 dimensional array.
    You can use print_out_layers turn off verbosity.
    """
    # Initialising the RNN
    model = Sequential()
    # Adding the first RNN layer
    model.add(SimpleRNN(units=10, activation='tanh', return_sequences=True,
                        input_shape=(x_train.shape[1], 1)))
    # Adding a second RNN layer
    model.add(SimpleRNN(units=10))
    # Adding the output layer
    model.add(Dense(units=1))
    # Compiling the RNN
    model.compile(optimizer='adam', loss='mean_squared_error')
    # Fitting the RNN to the Training set
    model.fit(x_train, y_train, epochs=500, batch_size=128, verbose=False)

    if print_out_layers:
        # Show the summary of the model
        model.summary()
        # show some layer values
        model.get_layer('simple_rnn').get_weights()
        model.get_layer('simple_rnn_1').get_weights()
    return model


def get_recurrent_forecast(current_x: np.array, model: Sequential):
    current_x = np.reshape(current_x, (1, 3, 1))
    predictions = []
    for i in range(20):
        current_prediction = float(model.predict(current_x))
        predictions.append(current_prediction)
        current_x = np.reshape(np.append(current_x[:, 1:, :], current_prediction), (1, 3, 1))

    y_recurrent_predictions = np.array(predictions).reshape(-1, 1)
    return y_recurrent_predictions


def assess_the_accuracy_of_estimate(y_test, y_pred):
    print('********* Evaluating Forecast *****************')
    print('Scale of the price actual: {:.2f}'.format(float(y_test[0])))
    print('mean_squared_error: {:.2f}'.format(np.sqrt(mean_squared_error(y_test, y_pred))))
    print('mean_absolute_error: {:.2f}'.format(mean_absolute_error(y_test, y_pred)))
    print('median_absolute_error: {:.2f}'.format(median_absolute_error(y_test, y_pred)))
    print('max_error: {:.2f}'.format(max_error(y_test, y_pred)))
    print('explained_variance_score: {:.2f}'.format(np.sqrt(explained_variance_score(y_test, y_pred))))
    print('r2_score: {:.2f}'.format(r2_score(y_test, y_pred)))


def main():

    path = '/home/rad/Projects/python_assignment/data/sp_5y/individual_stocks_5yr/'

    price_type = 'close'
    df = pd.read_csv(path + 'GOOG_data.csv', index_col=0,
                     usecols=['date', price_type], nrows=100)
    print('The daily {} prices span from {} to {}.'.format(price_type, df.index[0], df.index[-1]))

    x_train, y_train, x_test, y_test, scaler = prepare_data(df)

    model = fit_rnn(x_train, y_train)
    # Getting the predicted stock price
    y_pred = model.predict(x_test)

    # Plot results
    y_pred = scaler.inverse_transform(y_pred)
    y_test = scaler.inverse_transform(y_test)
    y_train = scaler.inverse_transform(y_train.reshape(-1, 1))
    # Check the accuracy of the forecast
    plot_results(y_test, y_pred)
    plot_train_series_and_results(y_train, y_test, y_pred)
    assess_the_accuracy_of_estimate(y_test, y_pred)

    y_recurrent_predictions = get_recurrent_forecast(x_train[-1], model)
    y_recurrent_predictions = scaler.inverse_transform(y_recurrent_predictions)

    plot_results(y_test, y_recurrent_predictions)
    plot_train_series_and_results(y_train, y_test, y_recurrent_predictions)
    assess_the_accuracy_of_estimate(y_test, y_recurrent_predictions)

    return 0


if __name__ == '__main__':
    main()
