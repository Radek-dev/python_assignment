"""
Simple tensorflow.keras neural network class
Only single time series are considered
"""

import numpy as np
import pandas as pd
import seaborn as sns

from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import (
    explained_variance_score,
    mean_squared_error,
    mean_absolute_error,
    median_absolute_error,
    r2_score,
    max_error)

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import (
    Dense,
    SimpleRNN,
    Dropout)

sns.set_style('darkgrid')
plt.rcParams.update({'font.size': 25})


class RnnWrapper:
    """
    df = None
    scaler = None
    series_scaled = None
    lags = None
    x_train = None
    y_train = None
    x_test = None
    y_test = None
    n = None
    n_train = None
    model = None
    one_step_forecast = None
    y_pred = None
    y_test_no_scale = None
    y_train_no_scale = None
    y_recurrent_pred = None

    """
    df = None
    scaler = None
    series_scaled = None
    lags = None
    x_train = None
    y_train = None
    x_validate = None
    y_validate = None
    x_test = None
    y_test = None
    n = None
    n_train = None
    n_validate = None
    model = None
    one_step_forecast = None
    y_pred = None
    y_train_no_scale = None
    y_validate_no_scale = None
    y_test_no_scale = None
    y_recurrent_pred = None

    def __init__(self):
        pass

    def load_close_prices_simple(self, path: str = '', csv_name: str = ''):
        if not path:
            path = '/home/rad/Projects/python_assignment/data/sp_5y/individual_stocks_5yr/'
        if not csv_name:
            csv_name = 'GOOG_data.csv'
        self.df = pd.read_csv(path + csv_name, index_col=0,
                              usecols=['date', 'close'], nrows=200)
        print('The daily close prices from {} to {} loaded.'.format(
            self.df.index[0], self.df.index[-1]))
        self.prepare_data()

    def load_close_prices_all(self, path: str = '', csv_name: str = ''):
        if not path:
            path = '/home/rad/Projects/python_assignment/data/sp_5y/individual_stocks_5yr/'
        if not csv_name:
            csv_name = 'GOOG_data.csv'
        self.df = pd.read_csv(path + csv_name, index_col=0,
                              usecols=['date', 'close'])
        print('The daily close prices from {} to {} loaded.'.format(
            self.df.index[0], self.df.index[-1]))

    def prepare_data(self, lags: int = 3, train_percentage: float = .7,
                     validation_percentage: float = 0.20, scaler_range: tuple = (-1, 1),
                     plotting: bool = True):
        self.lags = lags
        self.n = round(self.df.shape[0])
        self.n_train = round(self.n * train_percentage) - self.lags
        self.n_validate = self.n_train + round(self.n * validation_percentage)

        # Scale the features
        self.scaler = MinMaxScaler(feature_range=scaler_range)
        self.series_scaled = self.scaler.fit_transform(self.df.values)

        # prepare the lags and the shape of the data
        x, y = [], []
        for i in range(self.lags, self.n):
            x.append(self.series_scaled[i - self.lags:i, 0])
            y.append(self.series_scaled[i, 0])
        x, y = np.array(x), np.array(y)
        x = np.reshape(x, (x.shape[0], x.shape[1], 1))

        # split the dateset to train, validation and test sets
        self.x_train, self.y_train = x[:self.n_train, :, :], y[:self.n_train]
        self.x_validate = x[self.n_train:self.n_validate, :, :]
        self.y_validate = y[self.n_train:self.n_validate]
        self.x_test, self.y_test = x[self.n_validate:, :, :], y[self.n_validate:]

        self.y_validate = self.y_validate.reshape(-1, 1)
        self.y_test = self.y_test.reshape(-1, 1)

        # prepare the sets unscaled
        self.y_train_no_scale = self.scaler.inverse_transform(self.y_train.reshape(-1, 1))
        self.y_validate_no_scale = self.scaler.inverse_transform(self.y_validate)
        self.y_test_no_scale = self.scaler.inverse_transform(self.y_test)

        if plotting:
            self.plot_series()

    def fit_simple_rnn(self, verbose: bool = True):
        # Initialising the RNN
        self.model = Sequential()
        # Adding the first RNN layer
        self.model.add(SimpleRNN(units=10, activation='tanh',
                                 return_sequences=True, input_shape=(3, 1)))
        # Adding a fourth RNN layer
        self.model.add(SimpleRNN(units=10))

        self.model.add(SimpleRNN(units=10))

        self.model.add(SimpleRNN(units=10))
        # Adding the output layer
        self.model.add(Dense(units=1))
        # Compiling the RNN
        self.model.compile(optimizer='adam', loss='mean_squared_error')
        # Fitting the RNN to the Training set
        self.model.fit(self.x_train, self.y_train, epochs=50,
                       batch_size=32, verbose=verbose)

        if verbose:
            # Show the summary of the model
            self.model.summary()
            # show some layer values
            self.model.get_layer('simple_rnn').get_weights()
            self.model.get_layer('simple_rnn_1').get_weights()

    def fit_simple_rnn_sequence(self, verbose: bool = True):
        self.model = Sequential()
        self.model.add(SimpleRNN(units=30, activation='tanh',
                                 return_sequences=True, input_shape=(None, 1)))
        self.model.add(SimpleRNN(units=30))
        self.model.add(Dense(units=20))
        self.model.compile(optimizer='adam', loss='mean_squared_error')
        self.model.fit(self.x_train, self.y_train, epochs=50, verbose=verbose,
                       validation_data=(self.x_validate, self.y_validate))
        if verbose:
            self.model.summary()

    def fit_rnn(self, verbose: bool = True):
        # Initialising the RNN
        self.model = Sequential()
        # 1st RNN layer
        self.model.add(SimpleRNN(units=50, activation='relu', return_sequences=True,
                                 input_shape=(self.x_train.shape[1], 1)))
        # 2nd RNN layer
        self.model.add(SimpleRNN(units=45, activation='relu', return_sequences=True))

        # 3rd RNN layer and some Dropout regularisation
        self.model.add(SimpleRNN(units=40, activation='relu', return_sequences=True))
        self.model.add(Dropout(0.2))

        # 4th RNN layer and some Dropout regularisation
        self.model.add(SimpleRNN(units=30, activation='relu'))
        self.model.add(Dropout(0.2))
        # the output layer
        self.model.add(Dense(units=1))
        # Compiling the RNN
        self.model.compile(optimizer='adam', loss='mean_squared_error')
        # Fitting the RNN to the Training set
        self.model.fit(self.x_train, self.y_train,
                       epochs=128, batch_size=128, verbose=True)
        if verbose:
            self.model.summary()

    def show_benchmark_forecast(self):
        y_pred_naive = np.append(self.y_train[-1], self.y_validate[:-1]).reshape(-1, 1)
        y_pred_naive = self.scaler.inverse_transform(y_pred_naive)
        self.plot_results(y_pred_naive)
        self.plot_train_series_and_results(y_pred_naive)
        self.assess_forecast(y_pred_naive, model='Naive')

    def get_few_step_ahead_forecast(self):
        y_pred_seq = self.model.predict(self.x_train[-1:, ...])
        y_pred_seq = self.scaler.inverse_transform(y_pred_seq).reshape(-1, 1)
        self.plot_results(y_pred_seq)

    def get_one_step_forecast(self):
        y_pred = self.model.predict(self.x_validate)
        self.y_pred = self.scaler.inverse_transform(y_pred)
        self.plot_results(self.y_pred)
        self.plot_train_series_and_results(self.y_pred)
        self.assess_forecast(self.y_pred, model='Simple_RNN')

    def assess_forecast(self, y_pred: np.array, model: str):
        print('********* Evaluating Forecast - ' + model + ' *****************')
        print('Scale of the price actual: {:.2f}'.format(
            float(self.y_validate_no_scale[-1])))
        print('mean_squared_error: {:.2f}'.format(
            np.sqrt(mean_squared_error(self.y_validate_no_scale, y_pred))))
        print('mean_absolute_error: {:.2f}'.format(
            mean_absolute_error(self.y_validate_no_scale, y_pred)))
        print('median_absolute_error: {:.2f}'.format(
            median_absolute_error(self.y_validate_no_scale, y_pred)))
        print('max_error: {:.2f}'.format(
            max_error(self.y_validate_no_scale, y_pred)))
        print('explained_variance_score: {:.2f}'.format(
            np.sqrt(explained_variance_score(self.y_validate_no_scale, y_pred))))
        print('r2_score: {:.2f}'.format(
            r2_score(self.y_validate_no_scale, y_pred)))

    def plot_series(self):
        """
        This plots series y_test after series y_train.
        """
        nans_validate = np.empty(self.y_train.shape)
        nans_validate.fill(np.nan)
        y_validate = np.append(nans_validate, self.y_validate)
        nans_test = np.empty(y_validate.shape)
        nans_test.fill(np.nan)
        y_test = np.append(nans_test, self.y_test)
        plt.subplots(figsize=(30, 7))
        plt.plot(self.y_train, color='blue', label='Training Set')
        plt.plot(y_validate, color='red', label='Validation Set')
        plt.plot(y_test, color='purple', label='Test Set')
        plt.title('Scaled Alphabet Inc Daily Close')
        plt.xlabel('Time')
        plt.ylabel('Scaled Close Price')
        plt.legend()
        plt.show()

    def plot_results(self, y_pred: np.array):
        """
        This plots two series with the same time index.
        """
        plt.subplots(figsize=(30, 10))
        plt.plot(self.y_validate_no_scale, color='red',
                 marker='o', label='Validation Set', linewidth=5, markersize=15)
        plt.plot(y_pred, color='green', marker='x', label='Forecasted set', linewidth=5, markersize=15)
        plt.title('Validation and One-Period Forecasted Sets')
        plt.xlabel('Time')
        plt.ylabel('Unscaled Close Price')
        plt.legend()
        plt.show()

    def plot_train_series_and_results(self, y_pred: np.array):
        nans = np.empty(self.y_train.shape)
        nans.fill(np.nan)
        y_test = np.append(nans, self.y_validate_no_scale)
        y_pred = np.append(nans, y_pred)

        plt.subplots(figsize=(30, 10))
        plt.plot(self.y_train_no_scale, color='blue', label='Training Series', linewidth=5)
        plt.plot(y_test, color='red', label='Validation Set', linewidth=5, marker='o', markersize=15)
        plt.plot(y_pred, color='green', label='Forecasted set', linewidth=5, marker='x', markersize=15)
        plt.title('Training, Validation and Forecasted Sets')
        plt.xlabel('Time')
        plt.ylabel('Unscaled Close Price')
        plt.legend()
        plt.show()


def main():
    rnn_wrapper_simple = RnnWrapper()
    rnn_wrapper_simple.load_close_prices_simple()
    rnn_wrapper_simple.fit_simple_rnn()
    rnn_wrapper_simple.show_benchmark_forecast()
    rnn_wrapper_simple.get_one_step_forecast()

    rnn_wrapper_simple_sequence = RnnWrapper()
    rnn_wrapper_simple_sequence.load_close_prices_simple()
    rnn_wrapper_simple_sequence.prepare_data(lags=50)
    rnn_wrapper_simple_sequence.fit_simple_rnn_sequence()
    rnn_wrapper_simple_sequence.get_few_step_ahead_forecast()
    return 0


if __name__ == '__main__':
    main()
